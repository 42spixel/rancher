# **Monitor**

Rancher server 1.6 for Docker.

### **Credits**

+ [rancher/server](https://hub.docker.com/r/rancher/server/)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/rancher/src/master/LICENSE.md)**.
